package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class FramesPageMainFrame {
    WebDriver driver;
    public FramesPageMainFrame (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
